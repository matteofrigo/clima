# clima

This repository contains the source code of the Command Line Interface MAtrix calculator (Clima).

Clima is a tool that allows to do matrix operations via command line. It takes as input matrices saved in text format and it uses the reverse polish notation.

## Example

Let file `m1.txt` be:

```text
1. 2.
3. 4.
```

and let file `m2.txt` be:

```text
1 10
100 1000
```

The element-wise multiplication between the two matrices can be computed via

```bash
clima m1.txt m2.txt -emult result.txt
```

where `result.txt` will be a text file with entries

```text
1. 20.
300. 4000.
```

## Development

The `master` branch is for stable versions and new features must first be merged and tested in the `devel` branch.

Sources go in `src`, tests go in `tests` and the documentation goes in `doc`.

To compile the program, run `make` in the root directory of the repository.
